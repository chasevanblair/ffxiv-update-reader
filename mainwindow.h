#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkReply>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private:
    Ui::MainWindow *ui;
    void processData();
    void dataReady();
    QNetworkReply * r;
    QByteArray * dataBuffer;
    int index;
    int len;
    bool firstRun;
private slots:
    void updateText();
    void prevClick();
    void nextClick();
    void openLink();
};
#endif // MAINWINDOW_H
