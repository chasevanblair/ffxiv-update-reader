#ifndef WORKER_H
#define WORKER_H
#include <QByteArray>
#include <QLabel>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>

class Worker : public QObject
{
public:
    //qurl in constructor
    Worker(QUrl address);
    Worker();
    //starts the get and parse chain
    QJsonArray retrieve();
    //url is the original address used to get data
    void setUrl(QUrl);
    QUrl getUrl();
    void makeReply();
    QNetworkAccessManager qnam;
    void dataReady();
    void dataFinished();
    QByteArray* data;
    //postUrl is the currently viewed post index in the ui
    void setPostUrl(QUrl);
    QUrl getPostUrl();
private slots:
    void OnViewClicked();

private:
    QUrl postUrl;
    QUrl mURL;
    void getJSON();
    void parseJSON();

};
#endif // WORKER_H
