/*Chase Van Blair
 * 8/5/2021
 * Advanced c++
 * Final semester project
 * crvanblair@dmacc.edu
 *
 *
 * This is a project that pulls the latest news from the MMO game FFXIV's website
 * https://na.finalfantasyxiv.com/lodestone/
 * This is completed using the QNetwork functions to pull the json api from the community run api:
 * https://documenter.getpostman.com/view/1779678/TzXzDHVk#58138291-1212-7b3b-a438-a9628393418b
 * Then that JSON data is parsed and used with the QJson libraries
 */
#include "worker.h"
#include "mainwindow.h"
#include <QByteArray>
#include <QLabel>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QObject>

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>

QUrl mUrl;
QUrl postUrl;
QByteArray *data;
QNetworkReply *netReply;
//this will do the requests
Worker::Worker(QUrl address)
{
    mUrl = address;

}
Worker::Worker(){

}

//stores url for networkreply
void Worker::setUrl(QUrl q)
{
    mUrl = q;
}
QUrl Worker::getUrl()
{
    return mUrl;
}





