/*Chase Van Blair
 * 8/5/2021
 * Advanced c++
 * Final semester project
 * crvanblair@dmacc.edu
 *
 *
 * This is a project that pulls the latest news from the MMO game FFXIV's website
 * https://na.finalfantasyxiv.com/lodestone/
 * This is completed using the QNetwork functions to pull the json api from the community run api:
 * https://documenter.getpostman.com/view/1779678/TzXzDHVk#58138291-1212-7b3b-a438-a9628393418b
 * Then that JSON data is parsed and used with the QJson libraries
 */
#include <QCoreApplication>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QUrl>
#include <QNetworkReply>
#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

//    QNetworkAccessManager qnam;
//    QUrl address("https://jsonplaceholder.typicode.com/posts");
//    QNetworkRequest request(address);

    return a.exec();
}
