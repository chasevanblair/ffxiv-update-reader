/*Chase Van Blair
 * 8/5/2021
 * Advanced c++
 * Final semester project
 * crvanblair@dmacc.edu
 *
 *
 * This is a project that pulls the latest news from the MMO game FFXIV's website
 * https://na.finalfantasyxiv.com/lodestone/
 * This is completed using the QNetwork functions to pull the json api from the community run api:
 * https://documenter.getpostman.com/view/1779678/TzXzDHVk#58138291-1212-7b3b-a438-a9628393418b
 * Then that JSON data is parsed and used with the QJson libraries
 */
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "worker.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QtDebug>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <memory>
#include <QIODevice>
#include <QByteArray>
#include <iostream>
#include <QDesktopServices>
std::unique_ptr <Worker> w = std::make_unique<Worker>();
std::unique_ptr <QByteArray>dataBuffer  = std::make_unique<QByteArray>();
std::unique_ptr <QJsonDocument>parsedJson  = std::make_unique<QJsonDocument>();



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , r(nullptr)
    , dataBuffer(new QByteArray)

{
    index = 0;
    firstRun = true;
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    dataBuffer->clear();
    delete ui;
}

void MainWindow::dataReady(){
    dataBuffer->append(r->readAll());
    qDebug() << "dataready";
}

void MainWindow::processData(){
    //set txt contents
    //data to json array
    //display all
    ui->txtNews->clear();
    QStringList title;
    QStringList description;
    QStringList url;
    QJsonDocument parsed = QJsonDocument::fromJson(*dataBuffer);
    QJsonObject root = parsed.object();
    QJsonArray jsonArray = parsed.array();
    //set the string list values to be parallel for each post's data
    foreach (const QJsonValue & value, jsonArray) {
        QJsonObject obj = value.toObject();
        title.append(obj["title"].toString());
        description.append(obj["description"].toString());
        url.append(obj["url"].toString());
    }
    len = title.size();

    //set the text box data in ui
    //title
    ui->txtNews->append( title[index] + "\n");
    //description
    ui->txtNews->append(description[index] + "\n");
    //url
    ui->txtNews->append(url[index] + "\n");
    //seperator
    ui->txtNews->append("-------------------");


}
//keep track of location with a global index number
//subtract one if user clicks prev then run updatetext
//add one if user clicks next then run updatetext
//makes a worker to get and return the json things
void MainWindow::updateText(){
    if(firstRun){
        w->setUrl(QUrl("http://na.lodestonenews.com/news/topics"));
        r = w->qnam.get(QNetworkRequest(w->getUrl()));
        connect(r, &QNetworkReply::readyRead, this, &MainWindow::dataReady);
        connect(r, &QNetworkReply::finished, this, &MainWindow::processData);

    }else{
        MainWindow::processData();
    }
    //firstrun is used to make sure the program only fetches the data on the initial load
    //not after using the previous/next buttons
    firstRun = false;


    ui->btnLoad->hide();
}
//slot for when user clicks previous button, cycles index - 1 and hides if index value is too low
void MainWindow::prevClick(){
    if(ui->btnNext->isHidden()){
        ui->btnNext->show();
    }
    if(index > 0){
        index -= 1;
        MainWindow::updateText();}

    else{
        ui->btnPrev->hide();
    }
}
//slot for when user clicks next button, cycles index + 1 and hides if index value is too high

void MainWindow::nextClick(){
    if(ui->btnPrev->isHidden()){
        ui->btnPrev->show();
    }
    if(index < len-1){
    index += 1;
    MainWindow::updateText();
    }
    else if(ui->btnLoad->isHidden()){
        MainWindow::updateText();
    }
    if(index == len-1){
        ui->btnNext->hide();

    }
}
