This C++ program gets update news for the video game Final Fantasy 14 (FFXIV)
It gives the user a quick rundown of each topic along with a link to go to in order to learn more.

Game updates listed here: https://na.finalfantasyxiv.com/lodestone/news/
API used here: https://documenter.getpostman.com/view/1779678/TzXzDHVk#58138291-1212-7b3b-a438-a9628393418b